const express = require('express');

const dataEntry = require('./videoSchema');

const router = express.Router();
router.use(express.json())
router.get('/listing-video-data', (req,res)=>{

    dataEntry.find({}).select(['Title','_id','Genre','Distributors','Production','Poster'])
    .then((data)=>{
        console.time('data');
        res.json(data)
        console.timeEnd('data')
            
    })
    .catch((err)=>{
        console.log('error')
    })
})


module.exports = router;