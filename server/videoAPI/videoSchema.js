const mongoose = require('mongoose');


mongoose.connect('mongodb+srv://aeroplaymedia:aeroplaymedia@cluster0-m14z3.mongodb.net/AeroplayDataBase', {useNewUrlParser: true, useUnifiedTopology: true}).catch(()=>{
    console.log('error has occurred')
})
    const mongooseSchema = mongoose.Schema
    const addVideoSchema = new mongooseSchema({
        Title: {type: String, default: 'N.A'},
        Choice: {type: String, default: 'N.A'},
        Production: {type: String, default: 'N.A'},
        Labs: {type: String, default: 'N.A'},
        Distributors: {type: String, default: 'N.A'},
        Genre: {type: String, default: 'N.A'},
        Rating: {type: String, default: 'N.A'},
        Poster: {type: String, default: 'N.A'},
        Version: {type: String, default: 'N.A'},
        Drm: {type: String, default: 'N.A'},
        TheatricalReleaseDate: {type: String, default: 'N.A'},
        AirlineReleaseDate: {type: String, default: 'N.A'},
        Cast: {type: String, default: 'N.A'},
        Director: {type: String, default: 'N.A'},
        Language: {type: String, default: 'N.A'},
        Dubs: {type: String, default: 'N.A'},
        Subs: {type: String, default: 'N.A'},
        Country: {type: String, default: 'N.A'},
        Summary: {type: String, default: 'N.A'},
        TheatricalRuntime: {type: String, default: 'N.A'},
        EditedRuntime: {type: String, default: 'N.A'},
        Notes: {type: String, default: 'N.A'},
        Color: {type: String, default: 'N.A'},
        RottenTomatoScore: {type: String, default: 'N.A'},
        IMDBUserRating: {type: String, default: 'N.A'},
        IMDBSummary: {type: String, default: 'N.A'},
        IncludedRights: {type: String, default: 'N.A'},
        ExcludedRights: {type: String, default: 'N.A'},
        TargetAudience: {type: String, default: 'N.A'},
        AeroplayContentReview: {type: String, default: 'N.A'},
        BoxOffice: {type: String, default: 'N.A'},
        Awards: {type: String, default: 'N.A'},
        Nominations: {type: String, default: 'N.A'},
    })

module.exports  = mongoose.model('videoData', addVideoSchema);


