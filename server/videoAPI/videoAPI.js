const express = require('express');

// const mongoose = require('mongoose');

const addVideo = require('./videoSchema');


const router = express.Router();

router.post('/add-video', (req,res)=>{

    const newData = new addVideo({
        Title: req.body.Title,
        Choice: req.body.Choice,
        Production: req.body.Production,
        Labs: req.body.Labs,
        Distributors: req.body.Distributors,
        Genre: req.body.Genre,
        Rating: req.body.Rating,
        Poster: req.body.Poster,
        Version: req.body.Version,
        Drm: req.body.Drm,
        TheatricalReleaseDate: req.body.TheatricalReleaseDate,
        AirlineReleaseDate: req.body.AirlineReleaseDate,
        Cast: req.body.Cast,
        Director: req.body.Director,
        Language: req.body.Language,
        Dubs: req.body.Dubs,
        Subs: req.body.Subs,
        Country: req.body.Country,
        Summary: req.body.Summary,
        TheatricalRuntime: req.body.TheatricalRuntime,
        EditedRuntime: req.body.EditedRuntime,
        Notes: req.body.Notes,
        Color: req.body.Color,
        RottenTomatoScore: req.body.RottenTomatoScore,
        IMDBUserRating: req.body.IMDBUserRating,
        IMDBSummary: req.body.IMDBSummary,
        IncludedRights: req.body.IncludedRights,
        ExcludedRights: req.body.ExcludedRights,
        TargetAudience: req.body.TargetAudience,
        AeroplayContentReview: req.body.AeroplayContentReview,
        BoxOffice: req.body.BoxOffice,
        Awards: req.body.Awards,
        Nominations: req.body.Nominations
     });
    

    newData.save().then(() => console.log('Data has been stored'));
    res.json(req.body)
})


module.exports = router;