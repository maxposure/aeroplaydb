const mongoose = require('mongoose');


mongoose.connect('Private data', {useNewUrlParser: true, useUnifiedTopology: true}).catch(()=>{
    console.log('error has occurred')
})
    const mongooseSchema = mongoose.Schema
    const addAudio = new mongooseSchema({
        Album: {type: String, default: 'N.A'},
        Artist: {type: String, default: 'N.A'},
        Tracks: {type: String, default: 'N.A'},
        AlbumRuntime: {type: String, default: 'N.A'},
        TrackDuration: {type: String, default: 'N.A'},
        Genre: {type: String, default: 'N.A'},
        AlbumDescription: {type: String, default: 'N.A'},
        Language: {type: String, default: 'N.A'},
        Label: {type: String, default: 'N.A'},
        ReleaseDate: {type: String, default: 'N.A'},
        Poster: {type: String, default: 'N.A'}
    })

module.exports  = mongoose.model('audioData', addAudio);


