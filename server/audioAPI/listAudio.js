const express = require('express');

const dataEntry = require('./audioSchema');

const router = express.Router();
router.use(express.json())
router.get('/listing-audio-data', (req,res)=>{

    dataEntry.find({}).select(['Album','_id','Genre','Artist','Label','Poster'])
    .then((data)=>{
        console.time('data');
        res.json(data)
        console.timeEnd('data')
            
    })
    .catch((err)=>{
        console.log('error')
    })
})


module.exports = router;