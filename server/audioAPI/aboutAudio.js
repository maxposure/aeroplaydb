const express = require('express');

const dataEntry = require('./audioSchema');

const router = express.Router();

router.post('/about-audio-data', (req,res)=>{

    dataEntry.findOne({_id: req.body.id}).select(['-_id','-__v'])
    .then((data)=>{
        console.time('data');
        
        res.json(data)
        console.timeEnd('data')
            
    })
    .catch((err)=>{
        console.log('error')
    })

    // res.json(req.body)
})


module.exports = router;