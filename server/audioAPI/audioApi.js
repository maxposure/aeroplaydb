const express = require('express');

// const mongoose = require('mongoose');

const dataEntry = require('./audioSchema');


const router = express.Router();

router.post('/add-audio', (req,res)=>{

    const newData = new dataEntry({
        Album: req.body.Album,
        Artist: req.body.Artist,
        Tracks: req.body.Tracks,
        AlbumRuntime: req.body.AlbumRuntime,
        TrackDuration: req.body.TrackDuration,
        Genre: req.body.Genre,
        AlbumDescription: req.body.AlbumDescription,
        Language: req.body.Language,
        Label: req.body.Label,
        ReleaseDate: req.body.ReleaseDate,
        Poster: req.body.Poster
    });
    

    newData.save().then(() => console.log('Data has been stored'));
    res.json(req.body)
})


module.exports = router;