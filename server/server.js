const express = require('express');
const path = require('path');

const addVideoRoute = require('./videoAPI/videoAPI');
// const listingRoute = require('./listingAPI');
const listVideo = require('./videoAPI/listVideo');
const listAudio = require('./audioAPI/listAudio');
// const aboutMovieRoute = require('./aboutMovieAPI');
const addAudioRoute = require('./audioAPI/audioApi');

const aboutVideo = require('./videoAPI/aboutVideo')
const aboutAudio = require('./audioAPI/aboutAudio')

const app = express()
const port = 3000

app.use(express.json())

app.use('/',addVideoRoute, addAudioRoute, listVideo, listAudio, aboutVideo, aboutAudio);


app.use(express.static(path.resolve(__dirname,'./../dist')))

app.get('*', (req, res) => res.sendFile(path.resolve(__dirname,'./../dist/index.html')));


app.listen(port, () => console.log(`Example app listening on port ${port}!`))