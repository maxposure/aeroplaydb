import ReactDOM from "react-dom";
import React from 'react';
import './index.scss';
import './assets/styles/styles.scss';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import DataEntry from './Components/Video/Video';
import VideoList from './Components/Video/VideoList';
import AudioList from './Components/Audio/AudioList'
import Audio from './Components/Audio/Audio';
import AboutVideo from './Components/Video/AboutVideo';
import AboutAudio from './Components/Audio/AboutAudio';
import './assets/fontawesome/fontawesome';

function App() {
  return(
    <> 
    <Router>
      <div className="header">
        <div className="logo">
          <img src="https://res.cloudinary.com/dyqcevdpm/image/upload/v1528269110/aeroplay_bppt2c.png" alt="Aeroplay"/>
        </div>
        <nav>
          <ul>
            <li><Link to="/">Add Video</Link></li>
            <li><Link to="/add-audio">Add Audio</Link></li>
            <li><Link to="/video-list">Video List</Link></li>
            <li><Link to="/audio-list">Audio List</Link></li>
          </ul>
        </nav>
      </div>
      <Switch>
        <Route exact path="/">
          <DataEntry/>
        </Route>
        <Route path="/add-video">
          <DataEntry/>
        </Route>
        <Route path="/add-audio">
          <Audio/>
        </Route>
        <Route path="/video-list">
          <VideoList/>
        </Route>
        <Route path="/audio-list">
          <AudioList/>
        </Route>
        <Route path="/about-video">
          <AboutVideo/>
        </Route>
        <Route path="/about-audio">
          <AboutAudio/>
        </Route>
        
      </Switch>
    </Router>
    </>
  )
}

ReactDOM.render(<App />, document.querySelector("#index"));
