import React, {useState} from 'react';
import AudioModal from './AudioModal';

function Audio() {

    const [storeData, setStoreData] = useState({});
    let formData = {};
    const openModal =(e)=>{
        const modal = document.querySelector('.modal');
        modal.classList.add('is-active')
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let errorValue=false;
        const help = document.querySelectorAll('.help');

        for(let i=0; i<e.target.length-1;i++){
            if(e.target[i].type==='radio'){
                for(let j=0; j<document.getElementsByName(e.target[i].name).length; j++){
                    if(document.getElementsByName(e.target[i].name)[j].checked){
                        formData[e.target[i].name]=document.getElementsByName(e.target[i].name)[j].value;
                    }
                }
            }
            else{
                if(e.target[i].value===''||e.target[i].value===null){
                    e.target[i].parentElement.parentElement.lastChild.style.display="block";
                    
                }
                else{
                    e.target[i].parentElement.parentElement.lastChild.style.display="none";
                    formData[e.target[i].name]=e.target[i].value;
                }
            }
        }
        
        errorValue=false

        help.forEach((element)=>{
            if(element.style.display==='block'){
                errorValue=true
            }
        })

        


        if(errorValue===false){
            setStoreData(formData)
            
            openModal(formData);

        }
    }

    return (<>
    <section className="section">
        <div className="container">
            <AudioModal formData={storeData}/>
            <h1 className="title">
                Add Audio
            </h1>
            <form className="columns is-multiline" onSubmit={handleSubmit}>
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="album">Album</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Album's Name" id="album" name="Album"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="artist">Artist</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Artist's Name" id="artist" name="Artist"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="tracks">Track</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Track's Name" id="tracks" name="Tracks"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="albumRuntime">Album Runtime</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Album's Runtime" id="albumRuntime" name="AlbumRuntime"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="trackDuration">Track Duration</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Track Duration" id="trackDuration" name="TrackDuration"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="genre">Genre</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Genre" id="genre" name="Genre"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="albumDescription">Album Description</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Album's Description" id="albumDescription" name="AlbumDescription"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="language">Language</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter language" id="language" name="Language"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="labelName">Label</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Label" id="labelName" name="Label"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="releaseDate">Release Date</label>
                        <div className="control">
                            <input className="input" type="date" placeholder="Enter Release Date" id="releaseDate" name="ReleaseDate"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="poster">Poster</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Poster Url" id="poster" name="Poster"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <label className="label" style={{opacity: 0}}>Submit</label>
                    <button className="button is-info" style={{width: "300px"}}>submit</button>
                </div>
                
            </form>
        </div>
    </section>
    </>
    )
}

export default Audio
