import React, {useEffect, useState} from 'react'

function AboutAudio() {
    const [data,setData] = useState(null);

    useEffect(()=>{
        fetch('http://localhost:3000/about-audio-data', {
            method: 'POST',
            body: JSON.stringify({
            id: window.location.hash.substring(1)
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
        .then((res)=>res.json())
        .then((json)=>setData(json))
    },[]);

    return (
        <div className="movie-details-container">
            {
                data && <div className="movie-details">{
                    Object.keys(data).map((element)=><div className="movie-detail"><span>{element}</span><span><input type="text" value={data[element]} readOnly/></span></div>)
                }</div>
            }
        </div>
    )
}

export default AboutAudio
