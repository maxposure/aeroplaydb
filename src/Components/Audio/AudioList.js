import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom'
import './Listing.scss'

function Listing() {
    const [data,setData] = useState(null);

    useEffect(()=>{
        fetch('http://localhost:3000/listing-audio-data')
        .then(response => response.json())
        .then(json => setData(json))
        .catch(()=>{console.log('err')})
    },[]);


    return (
        <section className="section">
        {data &&
            <div className="container" style={{minWidth: "1000px"}}>
                <div className="columns is-centered is-mobile">
                    <div className="column has-text-weight-bold" style={{maxWidth: "60px"}}><span>S.No</span></div>
                    <div className="column is-2 has-text-weight-bold" style={{maxWidth: "100px"}}>Image</div>
                    <div className="column has-text-weight-bold">Album</div>
                    <div className="column is-2 has-text-weight-bold">Genre</div>
                    <div className="column is-2 has-text-weight-bold">Artist</div>
                    <div className="column is-2 has-text-weight-bold">Label</div>
                    <div className="column is-flex has-text-weight-bold" style={{maxWidth:"120px", justifyContent: "center"}}>View</div>
                </div>
                {
                    data.map((element,index)=><div className="columns is-mobile is-vcentered" key={index}>
                        <div className="column" style={{maxWidth: "60px"}}>{index+1}.</div>
                        <div className="column" style={{maxWidth: "100px"}}><img src={element["Poster"]} /></div>
                        <div className="column" >{element["Album"]}</div>
                        <div className="column is-2">{element["Genre"]}</div>
                        <div className="column is-2">{element["Artist"]}</div>
                        <div className="column is-2">{element["Label"]}</div>
                        <div className="column is-flex" style={{maxWidth:"120px", justifyContent: "center"}}><Link to={`/about-audio#${element["_id"]}`} className="button is-small is-rounded is-link">View More</Link></div>
                    </div>)
                }
            </div>
        }
        </section>
    );
}

export default Listing
