import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom'


function Listing() {
    const [data,setData] = useState(null);

    useEffect(()=>{
        fetch('http://localhost:3000/listing-video-data')
        .then(response => response.json())
        .then(json => setData(json))
        .catch(()=>{console.log('err')})
    },[]);


    return (
        <section className="section">
        {data &&
            <div className="container" style={{minWidth: "1000px"}}>
                <div className="columns is-centered is-mobile">
                    <div className="column has-text-weight-bold" style={{maxWidth: "60px"}}><span>S.No</span></div>
                    <div className="column is-2 has-text-weight-bold" style={{maxWidth: "100px"}}>Image</div>
                    <div className="column has-text-weight-bold">Title</div>
                    <div className="column is-2 has-text-weight-bold">Genre</div>
                    <div className="column is-2 has-text-weight-bold">Production</div>
                    <div className="column is-2 has-text-weight-bold">Distributor</div>
                    <div className="column is-flex has-text-weight-bold" style={{maxWidth:"120px", justifyContent: "center"}}>View</div>
                </div>
                {
                    data.map((element,index)=><div className="columns is-mobile is-vcentered" key={index}>
                        <div className="column" style={{maxWidth: "60px"}}>{index+1}.</div>
                        <div className="column" style={{maxWidth: "100px"}}><img src={element["Poster"]} /></div>
                        <div className="column" >{element["Title"]}</div>
                        <div className="column is-2">{element["Genre"]}</div>
                        <div className="column is-2">{element["Production"]}</div>
                        <div className="column is-2">{element["Distributors"]}</div>
                        <div className="column is-flex" style={{maxWidth:"120px", justifyContent: "center"}}><Link to={`/about-video#${element["_id"]}`} className="button is-small is-rounded is-link">View More</Link></div>
                    </div>)
                }
            </div>
        }
        </section>
    );
}

export default Listing
