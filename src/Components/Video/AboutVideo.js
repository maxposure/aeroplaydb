import React, {useEffect, useState} from 'react'

function AboutVideo() {
    const [data,setData] = useState(null);

    useEffect(()=>{
        fetch('http://localhost:3000/about-video-data', {
            method: 'POST',
            body: JSON.stringify({
            id: window.location.hash.substring(1)
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
        .then((res)=>res.json())
        .then((json)=>setData(json))
    },[]);



    return (
        <section className="section">
            <div className="container">
                {console.data}
            {
                data && <div className="columns is-multiline"><div className="column is-half">
                    <div className="field">
                        <label className="label">Title</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Title}/>
                        </div>
                    </div>
                </div>
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="choice">Choice:</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Choice}/>
                        </div>
                    </div>
                </div>
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="production">Select Production</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Production}/>
                        </div>
                    </div>
                </div>
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="labs">Select Labs</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Labs}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="distributors">Select Distributors</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Distributors}/>
                        </div>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="genre">Select Genre</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Genre}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="rating">Select Rating</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Rating}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="poster" className="label">Poster</label>
                        <figure class="image image is-3by2">
                            <img src={data.Poster}/>
                        </figure>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="version">Select Version</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Version}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label className="title is-5">DRM</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Drm}/>
                        </div>
                    </div>
                </div>

                
                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="theatricalReleaseDate" className="label">Theatrical Release Date</label>
                        <div className="control">
                            <input className="input" type="text" value={data.TheatricalReleaseDate}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="airlineReleaseDate" className="label">Airline Release Date</label>
                        <div className="control">
                            <input className="input" type="text" value={data.AirlineReleaseDate}/>
                        </div>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="enterCast" className="label">Cast</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Cast}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="director" className="label">Director</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Director}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="originalLanguage" className="label">Original Language</label>
                        <div className="control">
                            <input className="input" type="text" value={data.OriginalLanguage}/>
                        </div>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="originalLanguage" className="label">Available DUBS</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Dubs}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="originalLanguage" className="label">Available SUBS</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Subs}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="originalLanguage" className="label">Country Of Origin</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Country}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="summary" className="label">Summary</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Summary}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="theatricalRuntime" className="label">Theatrical Runtime</label>
                        <div className="control">
                            <input className="input" type="text" value={data.TheatricalRuntime}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="editedRuntime" className="label">Edited Runtime</label>
                        <div className="control">
                            <input className="input" type="text" value={data.EditedRuntime}/>
                        </div>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="notes" className="label">Notes</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Notes}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label className="title is-5">Color</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Color}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="rottenTomatoScore" className="label">Rotten Tomato Score</label>
                        <div className="control">
                            <input className="input" type="text" value={data.RottenTomatoScore}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="IMDBUserRating" className="label">IMDB User Rating</label>
                        <div className="control">
                            <input className="input" type="text" value={data.IMDBUserRating}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="IMDBSummary" className="label">IMDB Summary</label>
                        <div className="control">
                            <input className="input" type="text" value={data.IMDBSummary}/>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="includedRights" className="label">Included Rights</label>
                        <div className="control">
                            <input className="input" type="text" value={data.IncludedRights}/>
                        </div>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="excludedRights" className="label">Excluded Rights</label>
                        <div className="control">
                            <input className="input" type="text" value={data.ExcludedRights}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="targetAudience" className="label">Target Audience</label>
                        <div className="control">
                            <input className="input" type="text" value={data.TargetAudience}/>
                        </div>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="aeroplayContentReview" className="label">Aeroplay Content Review</label>
                        <div className="control">
                            <input className="input" type="text" value={data.AeroplayContentReview}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="boxOffice" className="label">Box Office</label>
                        <div className="control">
                            <input className="input" type="text" value={data.BoxOffice}/>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="awards" className="label">Awards</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Awards}/>
                        </div>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="nominations" className="label">Nominations</label>
                        <div className="control">
                            <input className="input" type="text" value={data.Nominations}/>
                        </div>
                    </div>
                </div>
                
                
            </div>
            }
        </div>
    </section>
    )
}

export default AboutVideo
