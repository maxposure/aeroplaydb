import React, {useState, useEffect} from 'react';


const videoModal = (props) => {


    const closeModal = ()=>{
        const modal = document.querySelector('.modal');
        modal.classList.remove('is-active')
    }

    const storeData = ()=>{
        fetch('http://localhost:3000/add-video', {
                method: 'POST',
                body: JSON.stringify(props.formData),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then((response)=>response.text())
            .then((json)=>location.reload())
            .catch(()=>{console.log('error')});
        
    }

    return ( <>
    <div className="modal">
        <div className="modal-background"></div>
        <div className="modal-card">
            <header className="modal-card-head">
                <p className="modal-card-title">Video Data Preview</p>
            </header>
            <section className="modal-card-body" style={{maxHeight:"calc(100vh - 160px)"}}>
            {
                Object.keys(props.formData).map((e,i)=><div className="columns" key={i}>
                    <div className="column is-narrow has-text-weight-bold">{e}:</div><div className="column is-half">{props.formData[e]}</div>
                </div>)
            }
            </section>
            <footer className="modal-card-foot">
                <button className="button is-danger" onClick={closeModal}>Cancel</button>
                <button className="button is-success" onClick={storeData}>Save</button>
            </footer>

        </div>
    </div>
    </> );
}
 
export default videoModal;