import React, {useState} from 'react';
import VideoModal from './VideoModal';

function DataEntry() {
    const [storeData, setStoreData] = useState({});
    let formData = {};
    const openModal =(e)=>{
        const modal = document.querySelector('.modal');
        modal.classList.add('is-active')
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let errorValue=false;
        const help = document.querySelectorAll('.help');

        for(let i=0; i<e.target.length-1;i++){
            if(e.target[i].type==='radio'){
                for(let j=0; j<document.getElementsByName(e.target[i].name).length; j++){
                    if(document.getElementsByName(e.target[i].name)[j].checked){
                        formData[e.target[i].name]=document.getElementsByName(e.target[i].name)[j].value;
                    }
                }
            }
            else{
                if(e.target[i].value===''||e.target[i].value===null){
                    e.target[i].parentElement.parentElement.lastChild.style.display="block";
                    
                }
                else{
                    e.target[i].parentElement.parentElement.lastChild.style.display="none";
                    formData[e.target[i].name]=e.target[i].value;
                }
            }
        }
        
        errorValue=false

        help.forEach((element)=>{
            if(element.style.display==='block'){
                errorValue=true
            }
        })

        if(errorValue===false){
            setStoreData(formData)
            openModal(formData);
        }
    }


    return (
    <section className="section">
        <div className="container">
            
            <VideoModal formData={storeData}/>




            <h1 className="title">
                Add Video
            </h1>
            <form className="columns is-multiline" onSubmit={handleSubmit}>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="title">Title</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="Enter Title" name="Title" id="title"/>
                        </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="choice">Make a choice:</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select name="Choice" defaultValue={null} id="choice">
                                    <option value="videos">Videos</option>
                                    <option value="movies">Movies</option>
                                </select>
                            </div>
                            <p className="help is-danger" style={{display:"none"}}></p>
                        </div>
                    </div>
                </div>
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="production">Select Production</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select name="Production" id="production">
                                    <option value="marvel studio">Marvel Studio</option>
                                    <option value="fox star">Fox Star</option>
                                    <option value="warner bros">Warner Bros</option>
                                </select>
                            </div>
                            <p className="help is-danger" style={{display:"none"}}></p>
                        </div>
                    </div>
                </div>
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="labs">Select Labs</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select name="Labs" id="labs">
                                    <option value="kodak films">Kodak Films</option>
                                    <option value="pme labs">PME Lab</option>
                                </select>
                            </div>
                            <p className="help is-danger" style={{display:"none"}}></p>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="distributors">Select Distributor</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select name="Distributors" id="distributors">
                                    <option value="madman entertainment">Madman Entertainment</option>
                                    <option value="paramount pictures">Paramount Pictures</option>
                                    <option value="dharma productions">Dharma Productions</option>
                                </select>
                            </div>
                            <p className="help is-danger" style={{display:"none"}}></p>
                        </div>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="genre">Select Genre</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select name="Genre" id="genre">
                                    <option value="comedy">Comedy</option>
                                    <option value="action">Action</option>
                                    <option value="adventure">Adventure</option>
                                    <option value="thriller">Thriller</option>
                                    <option value="drama">Drama</option>
                                </select>
                            </div>
                            <p className="help is-danger" style={{display:"none"}}></p>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="rating">Select Rating</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select name="Rating" id="rating">
                                    <option value="G">G</option>
                                    <option value="PG">PG</option>
                                    <option value="PG-13">PG-13</option>
                                    <option value="R">R</option>
                                    <option value="NC-17">NC-17</option>
                                    <option value="NR">NR</option>
                                    <option value="UR">UR</option>
                                </select>
                            </div>
                            <p className="help is-danger" style={{display:"none"}}></p>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="poster" className="label">Poster</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Poster Link" name="Poster" id="poster"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label className="label" htmlFor="version">Select Version</label>
                        <div className="control">
                            <div className="select is-fullwidth">
                                <select name="Version" id="version">
                                    <option value="Theatrical">Theatrical</option>
                                    <option value="Edited">Edited</option>
                                    <option value="Theatrical & Edited Both">Theatrical & Edited Both</option>
                                </select>
                            </div>
                            <p className="help is-danger" style={{display:"none"}}></p>
                        </div>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label className="title is-5">DRM</label>
                        <div className="control">
                            <label className="radio" htmlFor="drm">
                                <input type="radio" id="drm" name="Drm" value="DRM" defaultChecked/>
                                <span style={{margin: " 0 10px"}}>DRM</span>
                            </label>
                            <label className="radio" htmlFor="nonDrm">
                                <input type="radio" id="nonDrm" name="Drm" value="NON-DRM"/>
                                <span style={{margin: " 0 10px"}}>Non-DRM</span>
                            </label>
                        </div>
                    </div>
                </div>

                
                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="theatricalReleaseDate" className="label">Theatrical Release Date</label>
                            <div className="control">
                                <input className="input" type="date" name="TheatricalReleaseDate" id="theatricalReleaseDate"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="airlineReleaseDate" className="label">Airline Release Date</label>
                            <div className="control">
                                <input className="input" type="date" name="AirlineReleaseDate" id="airlineReleaseDate"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="enterCast" className="label">Cast</label>
                            <div className="control">
                                <input className="input" placeholder="Enter Cast" type="text" name="Cast" id="enterCast"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="director" className="label">Director</label>
                            <div className="control">
                                <input className="input" placeholder="Enter Director" type="text" name="Director" id="director"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="originalLanguage" className="label">Original Language</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Enter Original Language" name="Language" id="originalLanguage"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="originalLanguage" className="label">Available DUBS</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Enter Available DUBS" name="Dubs" id="availableDubs"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="originalLanguage" className="label">Available SUBS</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Enter Available SUBS" name="Subs" id="availableSubs"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="originalLanguage" className="label">Country Of Origin</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Enter Country Name" name="Country" id="originCountry"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="summary" className="label">Summary</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Summary" name="Summary" id="summary"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="theatricalRuntime" className="label">Theatrical Runtime</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Theatrical Runtime" name="TheatricalRuntime" id="theatricalRuntime"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="editedRuntime" className="label">Edited Runtime</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Edited Runtime" name="EditedRuntime" id="editedRuntime"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>
                
                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="notes" className="label">Notes</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Enter Notes" name="Notes" id="notes"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label className="title is-5">Color</label>
                        <div className="control">
                            <label className="radio" htmlFor="color">
                                <input type="radio" name="Color" value="Color" id="color" defaultChecked/>
                                <span style={{margin: " 0 10px"}}>Color</span>
                            </label>
                            <label className="radio" htmlFor="blackNwhite">
                                <input type="radio" name="Color" value="BlackNWhite" id="blackNwhite"/>
                                <span style={{margin: " 0 10px"}}>Black & White</span>
                            </label>
                        </div>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="rottenTomatoScore" className="label">Rotten Tomato Score</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Enter Rotten Tomato Score" name="RottenTomatoScore" id="rottenTomatoScore"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="IMDBUserRating" className="label">IMDB User Rating</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="IMDB User Rating" name="IMDBUserRating" id="IMDBUserRating"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="IMDBSummary" className="label">IMDB Summary</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="IMDB Summary" name="IMDBSummary" id="IMDBSummary"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="includedRights" className="label">Included Rights</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Included Rights" name="IncludedRights" id="includedRights"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="excludedRights" className="label">Excluded Rights</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Excluded Rights" name="ExcludedRights" id="excludedRights"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="targetAudience" className="label">Target Audience</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Target Audience" name="TargetAudience" id="targetAudience"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="aeroplayContentReview" className="label">Aeroplay Content Review</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Aeroplay Content Review" name="AeroplayContentReview" id="aeroplayContentReview"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="boxOffice" className="label">Box Office</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Box Office" name="BoxOffice" id="boxOffice"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>

                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="awards" className="label">Awards</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Awards" name="Awards" id="awards"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>



                <div className="column is-half">
                    <div className="field">
                        <label htmlFor="nominations" className="label">Nominations</label>
                            <div className="control">
                                <input className="input" type="text" placeholder="Nominations" name="Nominations" id="nominations"/>
                            </div>
                        <p className="help is-danger" style={{display:"none"}}>This field is empty</p>
                    </div>
                </div>


                <div className="column is-half">
                    <label className="label" style={{opacity: 0}}>Submit</label>
                    <button className="button is-info" style={{width: "300px"}}>submit</button>
                </div>
            </form>
        </div>
    </section>
    );
}

export default DataEntry;
